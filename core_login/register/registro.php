

<?php

      session_start();

      date_default_timezone_set("US/Central");

       if (!isset($_SESSION['usr_rol'])) {

       header('Location: ../../index.php'); 

          exit();

      }else{

     if ($_SESSION['usr_rol'] == "Admin") {

        #  If user isnt't admin can't see this page

      if(!isset($_SESSION['usuario'])) 

      {

          header('Location: ../../index.php'); 

          exit();

      }

    }elseif ($_SESSION['usr_rol'] == 'Usr') {

      header('Location: ../../index.php'); 

          exit();

    }

$usr= $_SESSION['usuario'];

$cash= $_SESSION['caja'];

}

      ?>

      

<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta name="description" content="SIRAS_SALES SYSTEM">

    <meta name="author" content="Avantec - elucio">

    <link rel="icon" href="">

    <title>SIRAS</title>

    <!-- Bootstrap core CSS -->

    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

    <link href="http://getbootstrap.com/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->

    <link href="../../css/dashboard.css" rel="stylesheet">

    <link href="../../css/main.css" rel="stylesheet">

    <!-- Normalize styles -->

    <link href="../../css/normalize.css" rel="stylesheet">

    <script src="../../js/jquery-3.1.1.min.js"></script>

    <script src="../../js/jquery-migrate-3.0.0.js"></script>

    <!-- data table-->

     <script type="text/javascript" src="../../js/tables/jquery.dataTables.min.js"></script>

    <link rel="stylesheet" type="text/css" href="../../js/tables/jquery.dataTables.min.css"/>

     <script type="text/javascript" src="../../js/tables/dataTables.buttons.min.js"></script>

     <script type="text/javascript" src="../../js/tables/pdfmake.min.js"></script>

    <script type="text/javascript" src="../../js/tables/vfs_fonts.js"></script>

    <script type="text/javascript" src="../../js/tables/js/buttons.html5.min.js"></script>

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">

      <div class="container-fluid">

        <div class="navbar-header">

          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

            <span class="sr-only">Toggle navigation</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

          </button>

          <a class="navbar-brand" href="#">SIRAS || <?php echo $usr;?> </a>

        </div>

        <div id="navbar" class="navbar-collapse collapse">

          <ul class="nav navbar-nav navbar-right">

            <li><a>Caja actual <strong>[<?php echo $cash;?>]</strong></a></li>

            <li><a href="../exit.php">Salir</a></li>

          </ul>

          <form action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?> method="POST" class="navbar-form navbar-right">

            <input type="text" class="form-control" name="id_cd" placeholder="Buscar..." autofocus>



          </form> 

        </div>

      </div>

    </nav>

    <div class="container-fluid">

      <div class="row">

        <div class="col-sm-3 col-md-2 sidebar">

          <ul class="nav nav-sidebar">

            <li ><a href="../../admin/cash_sales.php"><img src="../../pcs/glyphicons-132-inbox-plus.png"> Caja</a></li>

            <li><a href="../../admin/views/cash_close.php"><img src="../../pcs/glyphicons-209-cart-in.png"> Cerrar Caja</a></li>

            <li><a href="../../admin/views/sales.php"><img src="../../pcs/glyphicons-539-invoice.png"> Ventas por día </a></li>

            <li><a href="../../admin/views/cliente_deuda.php"><img src="../../pcs/glyphicons-539-invoice.png">Deudas Clientes</a></li>

             <li><a href="../../admin/costumers.php"><img src="../../pcs/glyphicons-44-group.png"> Clientes </a></li>

             <li><a href="../../admin/query_view/querys.php"><img src="../../pcs/glyphicons-46-calendar.png"> Reportes</a></li>

          </ul>

        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

          <h1 class="page-header">Usuarios</h1>

          <div class="row placeholders">

            <div id = "select_product" class="col-xs-6 col-sm-6 placeholder">

              <!--contenido-->

          

              <form method="post" class="form-horizontal">



                 <div class="form-group">    

                  <label class="control-label col-sm-2">Usuario</label>

                  <div class="col-sm-10"> 

                    <input class="form-control" name="usr" type="text" required>

                  </div>

                </div>



                 <div class="form-group">   

                  <label class="control-label col-sm-2">Contraseña</label>

                  <div class="col-sm-10"> 

                    <input class="form-control"  name = "pwd" type="password" required>

                  </div>

                </div>

                <div class="form-group">   

                  <label class="control-label col-sm-2">Caja</label>

                  <div class="col-sm-10"> 

                    <input class="form-control"  name = "caja" type="number" required>

                  </div>

                </div>



                <div class="form-group">

                <label class="control-label col-sm-2">Rol</label>

                

                <div class="col-sm-10">

                

                  <select class="form-control" name="rol">

                    <option value="Admin">Administrador</option>

                    <option value="Usr">Cajero</option>

                  </select>

                  </div>

               

                </div>



                  <button formaction="usr_new.php" type="submit" class="btn btn-primary btn-block">Registrar 

                  </button>

                  

               

            </form>

          

            

            </div>

            <div class="col-xs-6 col-sm-6 placeholder">

              <?php include("view_usr.php");?>

             </div>

            </div>

        </div>

      </div>

    </div>

    <!-- Modal -->





   



 

    <script>

$(document).ready(function() {

  

        $('#users').DataTable({

              "iDisplayLength": 2,

          "order": [[ 1, "asc" ]],

          "language": {

            "lengthMenu": 'Mostrar <select>'+

                  '<option value="10">10</option>'+

                  '<option value="20">20</option>'+

                  '<option value="30">30</option>'+

                  '<option value="40">40</option>'+

                  '<option value="50">50</option>'+

                  '<option value="-1">Todos</option>'+

                  '</select> Usuarios',

            "info": "Mostrando pagina _PAGE_ de _PAGES_",

            "infoEmpty": "No hay usuarios registrados",

            "infoFiltered": "(filtrada de _MAX_ registros)",

            "search": "Buscar:",

            "paginate": {

              "next":       "Siguiente",

              "previous":   "Anterior"

            },



          }

            

      

        });

  

    

      ;/*Use it for show more tables*/} );



      

    </script>

    <!-- Bootstrap core JavaScript

    ================================================== -->

    <!-- Placed at the end of the document so the pages load faster -->

    

    <script src="../../js/bootstrap.min.js"></script>

    <script src="../../js/main.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

    <script src="../../js/ie10-viewport-bug-workaround.js"></script>

  </body>

</html>

