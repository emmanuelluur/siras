
<?php 
date_default_timezone_set("US/Central");
require_once('../../siras/config.php');
if(!function_exists('mysqli_connect'))
{
    echo 'PHP cannot find the mysql extension. MySQL is required for run. Aborting.';
    exit();
}
$conn = @mysqli_connect($servername, $username, $password, $bd)
or die("Connection failed: " . mysqli_connect_error());
$sql = "SELECT * FROM users where estatus = 'active'";

$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
?>

<table id ="users" class="table table-hover">
<thead>
<tr>
  <th>Nombre</th>
  <th>Rol</th>
  <th>Caja</th>
  <th>Fecha Activo</th>
  <th>Acciones</th>
</tr>
</thead>
<?php
	while($row = mysqli_fetch_assoc($result)) {
	?>
<tr>
  <td><?php echo $row["usuario"];?></td>
  <td><?php if($row["rol"] == "Admin"){echo "Administrador";}else{echo "Cajero";}?></td>
  <td><?php echo $row["caja"];?></td>
  <td><?php echo $row["fecha_reg"];?></td>
  <td><a href="delete.php?id=<?php echo $row['id'];?>" target="_blank" onClick="window.open(this.href, this.target, 'width=900,height=400,resizable=no,left=400,top=200,screenX=400,screenY=200'); return false;window.focus();">Baja</a> || <a href="edit.php?id=<?php echo $row['id'];?>&usr=<?php echo $row['usuario'];?>&rol=<?php echo $row['rol'];?>&caja=<?php echo $row['caja'];?>&pwd=<?php echo $row['pasword'];?>" target="_blank" onClick="window.open(this.href, this.target, 'width=900,height=400,resizable=no,left=400,top=200,screenX=400,screenY=200'); return false;window.focus();">Modificar</a></td>
</tr>
<?php
}
?>
</table>
<?php
}else{echo mysqli_error($conn);}mysqli_close($conn);

?>