<?php 
session_start();
date_default_timezone_set("US/Central");
 //Vaciar sesión
 $_SESSION = array();
 //Destruir Sesión
 session_destroy();
 //Redireccionar a index.php
 header("location: ../index.php");
?>
