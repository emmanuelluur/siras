-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 15-08-2018 a las 12:54:41
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `siras_bd`
--
CREATE DATABASE IF NOT EXISTS `siras_bd` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `siras_bd`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers`
--

CREATE TABLE `customers` (
  `id_costumer` int(11) NOT NULL,
  `name` varchar(90) DEFAULT NULL,
  `id_card` varchar(50) DEFAULT NULL,
  `job_position` varchar(30) DEFAULT NULL,
  `turno` varchar(20) DEFAULT NULL,
  `credito` varchar(10) DEFAULT NULL,
  `status` varchar(30) DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Estructura de tabla para la tabla `horarios`
--

CREATE TABLE `horarios` (
  `id_hor` int(11) NOT NULL,
  `horario` varchar(50) DEFAULT NULL,
  `hora_ini` varchar(30) DEFAULT NULL,
  `hora_fin` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id_product` int(11) NOT NULL,
  `product` varchar(90) DEFAULT NULL,
  `code_bar` int(11) DEFAULT NULL,
  `price` varchar(90) DEFAULT NULL,
  `balance` varchar(90) DEFAULT NULL,
  `status` varchar(30) DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sales`
--

CREATE TABLE `sales` (
  `id_sale` int(11) NOT NULL,
  `caja` int(11) DEFAULT NULL,
  `usuario` varchar(90) DEFAULT NULL,
  `date_open` date DEFAULT NULL,
  `hour_open` varchar(30) DEFAULT NULL,
  `date_close` date DEFAULT NULL,
  `hour_close` varchar(30) DEFAULT NULL,
  `sart_sale` varchar(90) DEFAULT NULL,
  `venta_efe` varchar(90) DEFAULT NULL,
  `subsidios` varchar(90) DEFAULT NULL,
  `pago_tarj` varchar(90) DEFAULT NULL,
  `credito_oto` varchar(90) DEFAULT NULL,
  `credito_paga` varchar(90) DEFAULT NULL,
  `end_sale` varchar(90) DEFAULT NULL,
  `status` varchar(30) DEFAULT 'open'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Estructura de tabla para la tabla `sales_cash`
--

CREATE TABLE `sales_cash` (
  `id_salecash` int(11) NOT NULL,
  `ident` int(11) DEFAULT NULL,
  `caja` int(11) DEFAULT NULL,
  `usuario` varchar(90) DEFAULT NULL,
  `id_costumer` varchar(50) DEFAULT NULL,
  `costumer` varchar(90) DEFAULT NULL,
  `product` varchar(90) DEFAULT NULL,
  `debe` varchar(90) DEFAULT NULL,
  `date_open` date DEFAULT NULL,
  `hour_open` varchar(30) DEFAULT NULL,
  `tarjeta` varchar(20) DEFAULT NULL,
  `cant_tarjeta` varchar(90) DEFAULT NULL,
  `subsidio` varchar(20) DEFAULT NULL,
  `cant_subsidio` varchar(90) DEFAULT NULL,
  `subsidio_te` varchar(20) DEFAULT NULL,
  `efectivo` varchar(90) DEFAULT NULL,
  `venta_total` varchar(90) DEFAULT NULL,
  `deuda_pago` varchar(90) DEFAULT '0',
  `turno_sale` varchar(20) DEFAULT NULL,
  `turno_subsidio` varchar(30) DEFAULT 'Normal',
  `status` varchar(30) DEFAULT 'sale',
  `deuda_ptar` varchar(90) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Estructura de tabla para la tabla `subsidios`
--

CREATE TABLE `subsidios` (
  `id_sb` int(11) NOT NULL,
  `valor` varchar(30) DEFAULT '10.8',
  `status` varchar(30) DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `usuario` varchar(90) DEFAULT NULL,
  `pasword` varchar(90) DEFAULT NULL,
  `fecha_reg` date DEFAULT NULL,
  `hora_reg` varchar(30) DEFAULT NULL,
  `caja` int(11) DEFAULT NULL,
  `estatus` varchar(30) DEFAULT 'active',
  `rol` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id_costumer`);

--
-- Indices de la tabla `horarios`
--
ALTER TABLE `horarios`
  ADD PRIMARY KEY (`id_hor`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id_product`);

--
-- Indices de la tabla `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id_sale`);

--
-- Indices de la tabla `sales_cash`
--
ALTER TABLE `sales_cash`
  ADD PRIMARY KEY (`id_salecash`);

--
-- Indices de la tabla `subsidios`
--
ALTER TABLE `subsidios`
  ADD PRIMARY KEY (`id_sb`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `customers`
--
ALTER TABLE `customers`
  MODIFY `id_costumer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4036;
--
-- AUTO_INCREMENT de la tabla `horarios`
--
ALTER TABLE `horarios`
  MODIFY `id_hor` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sales`
--
ALTER TABLE `sales`
  MODIFY `id_sale` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6871;
--
-- AUTO_INCREMENT de la tabla `sales_cash`
--
ALTER TABLE `sales_cash`
  MODIFY `id_salecash` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=866050;
--
-- AUTO_INCREMENT de la tabla `subsidios`
--
ALTER TABLE `subsidios`
  MODIFY `id_sb` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
