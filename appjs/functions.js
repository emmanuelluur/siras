
//  muestra total a pagar
function ShowTotals(deuda) {
    total_deuda.innerHTML = "Total a pagar: $" + deuda;
}
//  muestra cambio efectivo
function Cambio(efectivo, deuda, subsidio) {
    let total = (efectivo + subsidio) - deuda;
    if (!subsi.checked) {
        total = efectivo - deuda;
    }
    console.log(total);
    
    if (total < 0.1) {
        cambio.innerHTML = "Cambio Efectivo: $ " + 0;

    } else {
        cambio.innerHTML = "Cambio Efectivo: $ " + (total.toFixed(2));
    }

}

//  efectivo recibido
function EfectivoRec(abono, efectivo) {
    let total = efectivo + abono;
    efectivo_recibido.innerHTML = "Efectivo Recibido $ " + (total);
}
//  Muestra total a pagar restando efectivo y pago con tarjeta
function TotalDeuda(tarjeta, efectivo, deuda, subsidio) {
    let total = deuda - (efectivo + tarjeta + subsidio);
    if (!subsi.checked) {
        total = deuda - (efectivo + tarjeta);
    }

    if (total < 1) {

        total_deuda.innerHTML = "Total a pagar: $ " + 0;
    } else {
        total_deuda.innerHTML = "Total a pagar: $ " + (total.toFixed(2));
    }
}

//  totla recibido tarjeta

function TarjetaRecibo(tarjeta, abonoTarjeta) {
    let total = tarjeta + abonoTarjeta;
    total_tarjetas.innerHTML = "Total Recibido Tarjeta: $ " + (total.toFixed(2));
}