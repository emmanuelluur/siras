<?php
      session_start();
      date_default_timezone_set("US/Central");
       if (!isset($_SESSION['usr_rol'])) {
       header('Location: ../../index.php'); 
          exit();
      }else{
     if ($_SESSION['usr_rol'] == "Usr") {
        #  If user isnt't admin can't see this page
      if(!isset($_SESSION['usuario'])) 
      {
          header('Location: ../../index.php'); 
          exit();
      }
    }elseif ($_SESSION['usr_rol'] == 'Admin') {
      header('Location: ../../index.php'); 
          exit();
    }
$usr= $_SESSION['usuario'];
$cash= $_SESSION['caja'];
}
      ?>


<!--deudas totales-->
<?php 

require_once('../../siras/config.php');
if(!function_exists('mysqli_connect'))
{
    echo 'PHP cannot find the mysql extension. MySQL is required for run. Aborting.';
    exit();
}
$conn = @mysqli_connect($servername, $username, $password, $bd)
or die("Connection failed: " . mysqli_connect_error());
$hoy= date("Y-m-d");
$fecha = date('Y-m-d');
    $nuevafecha = strtotime ( '-6 day' , strtotime ( $fecha ) ) ;
    $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
$sql = "SELECT sum(debe) - (sum(deuda_pago) + sum(deuda_ptar)) as pago, sum(debe) as db FROM sales_cash WHERE deuda_pago <>debe ";
$conta = 0;
$result = mysqli_query($conn, $sql);
                    if (mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $deuda= round($row["pago"],2);
                    $db= round($row["db"],2);
                    }else{  
                      echo "0";
                   }
                  mysqli_close($conn);
 ?>
 <!--pagos efectivo-->
<?php 

require_once('../../siras/config.php');
if(!function_exists('mysqli_connect'))
{
    echo 'PHP cannot find the mysql extension. MySQL is required for run. Aborting.';
    exit();
}
$conn = @mysqli_connect($servername, $username, $password, $bd)
or die("Connection failed: " . mysqli_connect_error());
$hoy= date("Y-m-d");
$fecha = date('Y-m-d');
    $nuevafecha = strtotime ( '-6 day' , strtotime ( $fecha ) ) ;
    $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
//end
$sql = "SELECT sum(deuda_pago) as efectivo FROM sales_cash WHERE deuda_pago <>debe and date_open BETWEEN '$nuevafecha' and '$fecha'";
$conta = 0;
$result = mysqli_query($conn, $sql);
                    if (mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $efe= round($row["efectivo"],2);
                    }else{  
                      echo "0";
                   }
                  mysqli_close($conn);
 ?>
  <!--pagos tar-->
<?php 

require_once('../../siras/config.php');
if(!function_exists('mysqli_connect'))
{
    echo 'PHP cannot find the mysql extension. MySQL is required for run. Aborting.';
    exit();
}
$conn = @mysqli_connect($servername, $username, $password, $bd)
or die("Connection failed: " . mysqli_connect_error());
$hoy= date("Y-m-d");
$fecha = date('Y-m-d');
    $nuevafecha = strtotime ( '-6 day' , strtotime ( $fecha ) ) ;
    $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
$sql = "SELECT sum(deuda_ptar) as tar FROM sales_cash WHERE deuda_pago <>debe and date_open BETWEEN '$nuevafecha' and '$fecha' ";
$conta = 0;
$result = mysqli_query($conn, $sql);
                    if (mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $tar= round($row["tar"],2);
                    }else{  
                      echo "0";
                   }
                  mysqli_close($conn);
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="SIRAS_SALES SYSTEM">
    <meta name="author" content="Avantec - elucio">
    <link rel="icon" href="">
    <title>SIRAS</title>
    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="http://getbootstrap.com/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../../css/dashboard.css" rel="stylesheet">
    <link href="../../css/main.css" rel="stylesheet">
    <!-- Normalize styles -->
    <link href="../../css/normalize.css" rel="stylesheet">
    <script src="../../js/jquery-3.1.1.min.js"></script>
    <script src="../../js/jquery-migrate-3.0.0.js"></script>
    <!-- data table-->
     <script type="text/javascript" src="../../js/tables/jquery.dataTables.min.js"></script>

    <link rel="stylesheet" type="text/css" href="../../js/tables/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../css/dataTables.bootstrap.min.css">
     <script type="text/javascript" src="../../js/tables/dataTables.buttons.min.js"></script>
     <script type="text/javascript" src="../../js/tables/pdfmake.min.js"></script>
    <script type="text/javascript" src="../../js/tables/vfs_fonts.js"></script>
    <script type="text/javascript" src="../../js/tables/js/buttons.html5.min.js"></script>

  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">SIRAS || <?php echo $usr;?> </a>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="">Caja actual <strong>[<?php echo $cash;?>]</strong></a></li>
            <li><a href="../../core_login/exit.php">Salir</a></li>
          </ul>
      
        </div>
      </div>
    </nav>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li ><a href="../cash_sales.php"><img src="../../pcs/glyphicons-132-inbox-plus.png"> Caja</a></li>
            <li><a href="cash_close.php"><img src="../../pcs/glyphicons-209-cart-in.png"> Cerrar Caja</a></li>
            <li ><a href="sales.php"><img src="../../pcs/glyphicons-539-invoice.png"> Ventas por día </a></li>
            <li class="active"><a href=""><img src="../../pcs/glyphicons-539-invoice.png">Deudas Clientes <span class="sr-only">(current)</span></a></li>
            <li><a href="../costumers.php"><img src="../../pcs/glyphicons-44-group.png"> Clientes</a></li>
            <li><a href="../query_view/querys.php"><img src="../../pcs/glyphicons-46-calendar.png"> Reportes</a></li>
            <li><a href="../query_view/qry_cred.php"><img src="../../pcs/glyphicons-46-calendar.png"> Historial Créditos</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 >Creditos actuales: <span class="alert alert-success">$ <?php echo $deuda;?></span></h1><br>
          <hr>
          <h5 >Pagos Efectivo: <span class="alert alert-info">$ <?php echo $efe;?></span>Pagos Tarjeta: <span class="alert alert-warning">$ <?php echo $tar;?></span></h5><br>

          <div class="row placeholders">
          
            <hr>
                <?php include("include_credi.php");?>
            
        </div>
      </div>
    </div>
  <script>
$(document).ready(function() {
  
        $('#sales').DataTable({
          "iDisplayLength": 10,
          "order": [[ 3, "desc" ]], //el valor numerico es la columna (0 a... n) 
          "language": {
             "lengthMenu": 'Mostrar <select>'+
             '<option value="5">5</option>'+
                  '<option value="10">10</option>'+
                  '<option value="20">20</option>'+
                  '<option value="30">30</option>'+
                  '<option value="40">40</option>'+
                  '<option value="50">50</option>'+
                  '<option value="-1">All</option>'+
                  '</select> Registros',
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtrada de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
              "next":       "Siguiente",
              "previous":   "Anterior"
            },

          }
           
        });
      ;/*Use it for show more tables*/} );

      
    </script>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/main.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
