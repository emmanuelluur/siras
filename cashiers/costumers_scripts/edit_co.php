<?php
      session_start();
      date_default_timezone_set("US/Central");
       if (!isset($_SESSION['usr_rol'])) {
       header('Location: ../../index.php'); 
          exit();
      }else{
     if ($_SESSION['usr_rol'] == 'Usr') {
        #  If user isnt't admin can't see this page
      if(!isset($_SESSION['usuario'])) 
      {
          header('Location: ../../index.php'); 
          exit();
      }
    }elseif ($_SESSION['usr_rol'] == 'Admin') {
      header('Location: ../../index.php'); 
          exit();
    }
$usr= $_SESSION['usuario'];
$cash= $_SESSION['caja'];
}
?>
<?php
$id=($_GET['id']);
$nombre=($_GET['nombre']);
$tipo=($_GET['tipo']);
$turno=($_GET['turno']);
$credito=($_GET['credito']);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="SIRAS_SALES SYSTEM">
    <meta name="author" content="Avantec - elucio">
    <link rel="icon" href="">
    <title>SIRAS</title>
    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="http://getbootstrap.com/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    
    <!-- Normalize styles -->
    <link href="../../css/normalize.css" rel="stylesheet">
    <style type="text/css">
      #ed{
        margin-left: auto;
  margin-right: auto;
  margin-top: 5%;
  margin-bottom: auto;

      }
    </style>
  </head>
<body>
  <div class="container">
    <div id = 'ed' class="panel panel-success">
      <div class="panel-heading"><h4>Información cliente <?php echo $nombre;?></h4></div>
      <div class="panel-body">
        <!--Formulario-->
        <form class="form-horizontal" action="" method="POST">
          <!--block-->
          <div class="form-group"><label class="control-label col-sm-2" for="t_empleado">Empleado categoria:</label>
          <div class="col-sm-10">
            <select class="form-control" id="cat_r" name="cat_emp">
              <option value='<?php echo $tipo;?>'><?php echo $tipo;?></option>
              <option value="sindicato">Sindicato</option>
              <option value="confianza">Confianza</option>
              <option value="practicante">Practicante</option>
            </select>
          </div></div>
          <!--block-->
          <div class="form-group"><label class="control-label col-sm-2" for="turn">Turno:</label>
          <div class="col-sm-10">
            <select class="form-control" id="trn" name="turno">
              <option value='<?php echo $turno;?>'><?php echo $turno;?></option>
              <option value="1">1er Turno</option>
              <option value="2">2do Turno</option>
              <option value="3">3er Turno</option>
            </select>
          </div></div>
           <!--block-->
          <div class="form-group"><label class="control-label col-sm-2" for="cred">Crédito:</label>
          <div class="col-sm-10">
            <select class="form-control" id="trn" name="cred">
              <option value='<?php echo $credito;?>'><?php echo $credito;?></option>
              <option value="No">No</option>
              <option value="Si">Si</option>
              
            </select>
          </div></div>
          <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-default btn-block" name="salvar">Guardar Cambios</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/jquery-migrate-3.0.0.js"></script>
    <script src="../js/bootstrap.min.js"></script>
</body>
</html>
<?php
if (isset($_POST['salvar'])) {
require_once('../../siras/config.php');
if(!function_exists('mysqli_connect'))
{
    echo 'PHP cannot find the mysql extension. MySQL is required for run. Aborting.';
    exit();
}
$conn = @mysqli_connect($servername, $username, $password, $bd)
or die("Connection failed: " . mysqli_connect_error());
$tip = $_POST['cat_emp'];
$turn = $_POST['turno'];
$credi = $_POST['cred'];

  $sql = "UPDATE customers SET 
              job_position='$tip',
              turno='$turn',
              credito='$credi'
           WHERE id_costumer ='$id' ";

if (mysqli_query($conn, $sql)) {
    echo "Record updated successfully";
} else {
    echo "Error updating record: " . mysqli_error($conn);
}


 ?>
  <script type="text/javascript">
    alert('Datos Guardados');
    window.close();
  </script>
 <?php
  mysqli_close($conn);
}
?>