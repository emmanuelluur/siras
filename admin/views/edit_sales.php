<?php
      session_start();
      date_default_timezone_set("US/Central");
       if (!isset($_SESSION['usr_rol'])) {
       header('Location: ../../index.php'); 
          exit();
      }else{
     if ($_SESSION['usr_rol'] == "Admin") {
        #  If user isnt't admin can't see this page
      if(!isset($_SESSION['usuario'])) 
      {
          header('Location: ../../index.php'); 
          exit();
      }
    }elseif ($_SESSION['usr_rol'] == 'Usr') {
      header('Location: ../../index.php'); 
          exit();
    }
$usr= $_SESSION['usuario'];
$cash= $_SESSION['caja'];
}
      ?>
 <?php
                    if (!isset($_POST['rep'])) {
                      # code...
                    }else{
                       $dtin= $_POST['inicio'];
                      $dtfin= $_POST['fin'];
                    }
                   ?>
      <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="SIRAS_SALES SYSTEM">
    <meta name="author" content="Avantec - elucio">
    <link rel="icon" href="">
    <title>SIRAS</title>
    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="http://getbootstrap.com/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../../css/dashboard.css" rel="stylesheet">
    <link href="../../css/main.css" rel="stylesheet">
    <!-- Normalize styles -->
    <link href="../../css/normalize.css" rel="stylesheet">
    <script src="../../js/jquery-3.1.1.min.js"></script>
    <script src="../../js/jquery-migrate-3.0.0.js"></script>
    <!-- data table-->
     <script type="text/javascript" src="../../js/tables/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../js/tables/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../css/dataTables.bootstrap.min.css">
     <script type="text/javascript" src="../../js/tables/dataTables.buttons.min.js"></script>
     <script type="text/javascript" src="../../js/tables/pdfmake.min.js"></script>
    <script type="text/javascript" src="../../js/tables/vfs_fonts.js"></script>
    <script type="text/javascript" src="../../js/tables/buttons.html5.min.js"></script>

  </head>
  <body>
  <div class="container">
  <form class="form-inline" method="POST" action="">
  <div class="form-group">
    <label for="ini">Fecha Inicio:</label>
    <input type="date" class="form-control" id="ini" name="inicio" value=<?php if (!isset($dtin)){echo date('Y-m-d');}else{echo $dtin;}?>>
  </div>
  <div class="form-group">
    <label for="fin">Fecha Final:</label>
    <input type="date" class="form-control" id="fn" name="fin" value=<?php if (!isset($dtfin)){echo date('Y-m-d');}else{echo $dtfin;}?>>
  </div>
 
  <button type="submit" class="btn btn-default" name="rep">Generar</button>
</form>
<hr>
 <?php
        if (isset($_POST['rep'])) {
          # code...
               require_once('../../siras/config.php');
                if(!function_exists('mysqli_connect'))
                  {
                    echo 'PHP cannot find the mysql extension. MySQL is required for run. Aborting.';
                    exit();
                  }
                  $dt1= $_POST['inicio'];
                  $dt2= $_POST['fin'];
                $conn = @mysqli_connect($servername, $username, $password, $bd)
                or die("Connection failed: " . mysqli_connect_error());
                $consulta = "SELECT * FROM sales_cash WHERE  date_open  between '$dt1' and '$dt2' ORDER BY date_open DESC";
                $result = mysqli_query($conn, $consulta);

                if (mysqli_num_rows($result) > 0) {
                    $i=0;
                     ?>
                  <table id="sales" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                  <tr>
                 <th>#</th>
                 <th>Cajero</th>
                 <th>Fecha y hora de venta</th>
                 
                 <th>Cliente</th>
                 <th>Venta total</th>
                 <th>Efectivo</th>
                 <th>Pago tarjeta</th>
                 <th>Abono efectivo</th>
                 <th>Abono tarjeta</th>
                 <th>Acciones</th>
                 
                  </tr>
                </thead>

                <tbody>
                      <?php
                    while($row = mysqli_fetch_assoc($result)) {
                         ?>
                    <tr>

                    <td><?php echo $i+=1;?></td>
                    <td><?php echo $row["usuario"];?></td>
                    <td><?php echo $row["date_open"];?> | <?php echo $row["hour_open"];?></td>
                    <td><?php echo $row["costumer"];?></td>
                    <td><?php echo $row["venta_total"];?></td>
                    <td><?php echo $row["efectivo"];?></td>
                    <td><?php echo $row["cant_tarjeta"];?></td>
                    <td><?php echo $row["deuda_pago"];?></td>
                    <td><?php echo $row["deuda_ptar"];?></td>
                     <td><a href="funct_editsales.php?id=<?php echo $row['id_salecash'];?>&deuda=<?php echo $row['deuda_pago'];?>&efectivo=<?php echo $row["efectivo"]?>&tar=<?php echo $row["deuda_ptar"];?>&ptar=<?php echo $row["cant_tarjeta"];?>" target="_blank" onClick="window.open(this.href, this.target, 'width=600,height=400,resizable=no,left=400,top=200,screenX=400,screenY=200'); return false;" class="btn btn-danger" name="edit">Editar</a></td>
                      
                    
                     
                     
                  </tr>
                    <?php
                    }
                } else {
                    echo "0 results";
                }

                mysqli_close($conn);
                }
         
            ?>
</div>
  </body>
  </html>