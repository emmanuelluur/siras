<?php
      session_start();
      date_default_timezone_set("US/Central");
       if (!isset($_SESSION['usr_rol'])) {
       header('Location: ../index.php'); 
          exit();
      }else{
     if ($_SESSION['usr_rol'] == "Admin") {
        #  If user isnt't admin can't see this page
      if(!isset($_SESSION['usuario'])) 
      {
          header('Location: ../index.php'); 
          exit();
      }
    }elseif ($_SESSION['usr_rol'] == 'Usr') {
      header('Location: ../index.php'); 
          exit();
    }
$usr= $_SESSION['usuario'];
$cash= $_SESSION['caja'];
}
      ?>
<?php
if (isset($_POST['salvar'])) {
require_once('../siras/config.php');
if(!function_exists('mysqli_connect'))
{
    echo 'PHP cannot find the mysql extension. MySQL is required for run. Aborting.';
    exit();
}
$conn = @mysqli_connect($servername, $username, $password, $bd)
or die("Connection failed: " . mysqli_connect_error());
$nombre = $_POST['nomb'];
$gafet = $_POST['gafet'];
$tip = $_POST['cat_emp'];
$turn = $_POST['turno'];
$credi = $_POST['cred'];

  $sql = "INSERT INTO customers(
              name,
              id_card,  
              job_position,
              turno,
              credito)
              VALUES('$nombre', '$gafet', '$tip', '$turn', '$credi')";

if (mysqli_query($conn, $sql)) {
    
    header('Location: costumers.php'); 
} else {
    echo "Error updating record: " . mysqli_error($conn);
}
  mysqli_close($conn);
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="SIRAS_SALES SYSTEM">
    <meta name="author" content="Avantec - elucio">
    <link rel="icon" href="">
    <title>SIRAS</title>
    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="http://getbootstrap.com/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">
    
    <!-- Normalize styles -->
    <link href="../css/normalize.css" rel="stylesheet">
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/jquery-migrate-3.0.0.js"></script>
    <!-- data table-->
     <script type="text/javascript" src="../js/tables/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../js/tables/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="../css/dataTables.bootstrap.min.css">
     <script type="text/javascript" src="../js/tables/dataTables.buttons.min.js"></script>
     <script type="text/javascript" src="../js/tables/pdfmake.min.js"></script>
    <script type="text/javascript" src="../js/tables/vfs_fonts.js"></script>
    <script type="text/javascript" src="../js/tables/buttons.html5.min.js"></script>

  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../core_login/register/registro.php">SIRAS || <?php echo $usr;?> </a>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="">Caja actual <strong>[<?php echo $cash;?>]</strong></a></li>
            <li><a href="../core_login/exit.php">Salir</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li ><a href="cash_sales.php"><img src="../pcs/glyphicons-132-inbox-plus.png"> Caja</a></li>
            <li><a href="views/cash_close.php"><img src="../pcs/glyphicons-209-cart-in.png"> Cerrar Caja</a></li>
            <li><a href="views/sales.php"><img src="../pcs/glyphicons-539-invoice.png"> Ventas por día </a></li>
            <li><a href="views/cliente_deuda.php"><img src="../pcs/glyphicons-539-invoice.png">Deudas Clientes</a></li>
             <li class="active"><a href=""><img src="../pcs/glyphicons-44-group.png"> Clientes <span class="sr-only">(current)</span></a></li>
             <li><a href="query_view/querys.php"><img src="../pcs/glyphicons-46-calendar.png"> Reportes</a></li>
             <li><a href="query_view/qry_cred.php"><img src="../pcs/glyphicons-46-calendar.png"> Historial Créditos</a></li>
             <li><a href="query_view/qry_sale.php"><img src="../pcs/glyphicons-46-calendar.png"> Historial Ventas</a></li>
          </ul>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1>Clientes</h1><br>
          <div class="row placeholders">
    <div id = 'ed' class="panel panel-primary">
      <div class="panel-heading"><h4>Registro cliente</h4></div>
      <div class="panel-body">
        <!--Formulario-->
        <form class="form-horizontal" action="" method="POST">
         <!--block-->
          <div class="form-group"><label class="control-label col-sm-2" for="card">Gafet:</label>
          <div class="col-sm-10">
            <input type="text" name="gafet" class="form-control" autofocus></div></div>
            <!--block-->
          <div class="form-group"><label class="control-label col-sm-2" for="name">Nombre:</label>
          <div class="col-sm-10">
            <input type="text" name="nomb" class="form-control" required></div></div>
          <!--block-->
          <div class="form-group"><label class="control-label col-sm-2" for="t_empleado">Empleado categoria:</label>
          <div class="col-sm-10">
            <select class="form-control" id="cat_r" name="cat_emp">
              
              <option value="sindicato">Sindicato</option>
              <option value="confianza">Confianza</option>
              <option value="practicante">Practicante</option>
            </select>
          </div></div>
          <!--block-->
          <div class="form-group"><label class="control-label col-sm-2" for="turn">Turno:</label>
          <div class="col-sm-10">
            <select class="form-control" id="trn" name="turno">
              
              <option value="1">1er Turno</option>
              <option value="2">2do Turno</option>
              <option value="3">3er Turno</option>
            </select>
          </div></div>
           <!--block-->
          <div class="form-group"><label class="control-label col-sm-2" for="cred">Crédito:</label>
          <div class="col-sm-10">
            <select class="form-control" id="trn" name="cred">
              
              <option value="No">No</option>
              <option value="Si">Si</option>
              
            </select>
          </div></div>
          <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-default btn-block" name="salvar">Guardar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
            
          </div>
          <div class="row placeholders">
        
          <br>
            <hr>
            <div id="contenido">
              <?php include('views/include_costumers.php');?>
            </div>
        </div>
      </div>
    </div>
  <script>
$(document).ready(function() {
  
        $('#costumers').DataTable({
          "iDisplayLength": 5,
          "order": [[ 1, "asc" ]],
          "language": {
             "lengthMenu": 'Mostrar <select>'+
             '<option value="5">5</option>'+
                  '<option value="10">10</option>'+
                  '<option value="20">20</option>'+
                  '<option value="30">30</option>'+
                  '<option value="40">40</option>'+
                  '<option value="50">50</option>'+
                  '<option value="-1">All</option>'+
                  '</select> Registros',
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtrada de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
              "next":       "Siguiente",
              "previous":   "Anterior"
            },

          }
           
        });
      ;/*Use it for show more tables*/} );

     


    </script>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/main.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>

